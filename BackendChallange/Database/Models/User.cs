using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendChallange.Database.Models
{
    public class User
    {
        public int Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UserName { get; set; }
        public List<PortfolioEntry> PortfolioEntries { get; set; }
    }
}